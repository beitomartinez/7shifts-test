<?php

namespace App;

use InvalidArgumentException;

class Calculator
{
    public function stringAdd(string $str) : int
    {
        $output = 0;

        // Split lines into "rows"
        $rows = explode('\n', $str);

        // Row delimeter
        $rowDelimiter = '/(,)/';
        if (preg_match("/^\/\/(.*)/", $rows[0], $matches) > 0) {
            $delimiters = explode(',', $matches[1]);
            $rowDelimiter = '/(' . implode('|', $delimiters) . ')/';

            array_splice($rows, 0, 1);
        }

        $negativeNumbers = [];

        // Sum each row and add it to big total
        $rowNumbers = 0;
        foreach ($rows as $row) {
            $rowNumbers = preg_split($rowDelimiter, $row);

            // Ignore numbers > 1000
            $rowNumbers = array_filter($rowNumbers, function ($number) {
                return $number <= 1000;
            });

            $output += (int) array_sum($rowNumbers);

            $negativeNumbers = array_merge(
                $negativeNumbers,
                array_filter($rowNumbers, function ($number) {
                    return $number < 0;
                })
            );
        }

        if (count($negativeNumbers) > 0) {
            throw new InvalidArgumentException(
                'Negatives not allowed! The following numbers caused this exception: '
                    . implode(', ', $negativeNumbers)
            );
        }

        return $output;
    }
}
