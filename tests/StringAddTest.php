<?php

namespace Tests;

use App\Calculator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class StringAddTest extends TestCase
{
    public function test_it_can_handle_multiple_delimiters()
    {
        $calculator = new Calculator;

        $result = $calculator->stringAdd('//aa,b,c\n6aa4b10\n5b5\n8aa2c');
        $expected = 40;

        $this->assertEquals($expected, $result);
    }

    public function test_it_ignores_large_numbers()
    {
        $calculator = new Calculator;

        $result = $calculator->stringAdd('4500,100,550\n10,1000');
        $expected = 1660;

        $this->assertEquals($expected, $result);
    }

    public function test_it_doesnt_allow_negative_numbers()
    {
        $this->expectException(InvalidArgumentException::class);
        
        $calculator = new Calculator;
        $calculator->stringAdd('42,-1,5,3,-7,');
    }

    public function test_it_supports_custom_delimiter()
    {
        $calculator = new Calculator;

        $result = $calculator->stringAdd('//_\n3_6_\n1_5\n10_5');
        $expected = 30;

        $this->assertEquals($expected, $result);
    }

    public function test_it_can_handle_line_breaks()
    {
        $calculator = new Calculator;

        $result = $calculator->stringAdd('7,2,,\n1,5\n10');
        $expected = 25;

        $this->assertEquals($expected, $result);
    }

    public function test_it_can_add_n_operands_comma_separated()
    {
        $calculator = new Calculator;

        $result = $calculator->stringAdd('4,2,6,8,');
        $expected = 20;

        $this->assertEquals($expected, $result);
    }
}
